﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3app2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {

            var navigationPage1 = new NavigationPage(new Tab1());
            navigationPage1.Icon = "icon.png";
            navigationPage1.Title = "Tab 1";

            
            Children.Add(navigationPage1);

            var navigationPage2 = new NavigationPage(new Tab2());
            navigationPage2.Icon = "icon.png";
            navigationPage2.Title = "Tab 2";

            
            Children.Add(navigationPage2);
        }
    }
}